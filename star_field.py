import tkinter
import random

# globalne premenne
CANVAS_WIDTH = 800
CANVAS_HEIGHT = 600
MIN_SPEED = -1
MAX_SPEED = 1

# priemer hviezdy, aj ked hviezda je kruznica
STAR_DIAMETER = 5 

def on_escape(event):
    root.destroy()


root = tkinter.Tk()
canvas = tkinter.Canvas(width=CANVAS_WIDTH, height=CANVAS_HEIGHT)
canvas.configure(bg="black")
canvas.pack()

root.bind("<Escape>", on_escape)

# trieda, ktora reprezentuje hviezdu
class Star:
    def __init__(self):

        # kazda hviezda zacina v strede platna
        self.x = CANVAS_WIDTH / 2
        self.y = CANVAS_HEIGHT / 2

        # rychlost hviezdy je v kazdej suradnici nezavisla
        self.speed_x = random.uniform(MIN_SPEED, MAX_SPEED)
        self.speed_y = random.uniform(MIN_SPEED, MAX_SPEED)

        self.ball = canvas.create_oval(self.x, self.y, self.x + STAR_DIAMETER, self.y + STAR_DIAMETER, fill="white", outline="white")

    # v kazdom kroku pohni hviezdou
    def tick(self):
        canvas.move(self.ball, self.speed_x, self.speed_y)

        # pokial sa hviezda dotyka okraju obrazovky,
        # vrat ju do stredu platna.
        if self.is_out():
            canvas.coords(self.ball, self.x, self.y, self.x + STAR_DIAMETER, self.y + STAR_DIAMETER)

    # vrati True, ak je hviezda mimo platna, inak vrati False
    def is_out(self):
        x_up, y_up, x_low, y_low = canvas.coords(self.ball)
        return x_up <= 0 or x_low >= CANVAS_WIDTH or y_up <= 0 or y_low >= CANVAS_HEIGHT

# vytvor 200 hviezd
stars = [Star() for _ in range(200)]

# ekvivalentne zapisu
# stars = []
# for _ in range(200):
#    stars.append(Star())

# nekonecna slucka
def loop():    
    for s in stars:
        s.tick()
    canvas.after(1, loop)

loop()
