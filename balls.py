import tkinter
import random

# rozmery platna, vsetko, co je napisane CAPS_LOCKOM_SNAKE_CASE
# (https://en.wikipedia.org/wiki/Snake_case), je globalna konstanta,
# teda hodnota, ktora sa nemeni. Ak by si niekedy citala kod,
# v ktorom by si videla nieco ako
#
# GLOBALNA_KONSTANTA = <neajaka hodnota>,
#
# tak nieco nie je v poriadku.
CANVAS_WIDTH = 800
CANVAS_HEIGHT = 600

# pocet lopticiek
NR_BALLS = 40

# minimalny a maximalny polomer lopty
MIN_RADIUS = 5
MAX_RADIUS = 40

# minimalna a maximalna rychlost lopty
MIN_SPEED = 0.2
MAX_SPEED = 2


# funkcia, ktora sa vola pri stlaceni tlacidla esc
def on_escape(event):

    # znic program
    root.destroy()


# vytvor odkaz na Tk interpret a korenove okno. Tk intepret
# je nieco ako virtualny pocitac, ktory kontroluje stlacenie klaves,
# vykreslovaciu slucku, stlacenie tlacidiel v appke a ine. Internet
# hovori, ze Tk interpret netreba vytvarat explicitne a ak ho nevytvorime,
# vytvori sa automaticky za nas. V tomto programe je vsak potrebny napr.
# na volanie .mainloop(). Toto je globalna premenna.
root = tkinter.Tk()

# vytvor kresliacu plochu
canvas = tkinter.Canvas(width=CANVAS_WIDTH, height=CANVAS_HEIGHT)

# cierne pozadie
canvas.configure(bg="black")

# commitni nastavenia canvasu
canvas.pack()

# povedz interpretu, ze ked sa stlaci tlacidlo Escape, nech zavola
# funckiu on_escape
root.bind('<Escape>', on_escape)


# trieda, ktora reprezentuje jednu lopticku
class Ball:

    # konstruktor, vytvori loptu s danym polomerom, poziciou a rychlostou
    def __init__(self, radius, position, speed):
        self.radius = radius
        self.position = position
        self.speed = speed
        self.ball = canvas.create_oval(
            self.position[0] - ball_radius,
            self.position[1] - ball_radius,
            self.position[0] + ball_radius,
            self.position[1] + ball_radius,
            fill="red",
            outline="grey"
        )

    # metoda (https://cs.wikipedia.org/wiki/Metoda_(programov%C3%A1n%C3%AD)), funkcia, ktora
    # sa vola v kazdom kroku programu. Neviem, kolko toho vies o metodach v Pythone, ale
    # kazda metoda musi mat aspon jeden argument s nazvom self, co moze byt divne, lebo
    # ked potomu metodu volas na objekte, napises lopta.tick(), ale bez akehokolvek
    # argumentu. S tym sa treba v Pythone zmierit.**
    def tick(self):

        # najprv pohni loptu v smere vektoru speed
        canvas.move(self.ball, self.speed[0], self.speed[1])

        # vyries kolizie
        # ziskaj poziciu laveho horneho a praveho spodneho stvoruholniku,
        # ktory ohranicuje loptu
        x_up, y_up, x_low, y_low = canvas.coords(self.ball)

        # pokial je pozicia vrchneho rohu v ktorejkolvek suradnici mensia ako 0,
        # lopta naburala bud do laveho okraju platna (x-sur. <= 0) alebo
        # vrchneho okraju platna (y-sur. <= 0). V kazdom pripade treba
        # otocit znamienko prislusnej rychlostnej suradnice. Pozri obrazok https://pasteboard.co/K8fv3O7.png.
        # Rovnako sa treba zachovat, ak lopta nabura do zvysnych dvoch okrajov platna
        if x_up <= 0 or x_low >= CANVAS_WIDTH:
            self.speed[0] = -self.speed[0]
        if y_up <= 0 or y_low >= CANVAS_HEIGHT:
            self.speed[1] = -self.speed[1]
        

# Globalna premenna, v ktorej su uchovane vsetky lopty
balls = []

# vytvor lopty. Pokial netreba je nazov iteracnej premennej nepodstatny,
# mozme ju nahradit symbolom _. Potom sa vsak nemozeme na premennu
# odkazovat v tele cyklu
for _ in range(NR_BALLS):

    # vyber nahodne cislo z intervalu [MIN_RADIUS, MAX_RADIUS],
    # to bude velkost polomeru lopty
    ball_radius = random.randint(MIN_RADIUS, MAX_RADIUS)

    # pre kazdu suradnicu vyberieme hodnotu z takeho intervalu,
    # aby pociatocna pozicia lopty nebola mimo obrazovky
    ball_position = [\
        random.uniform(ball_radius, CANVAS_WIDTH - ball_radius),\
        random.uniform(ball_radius, CANVAS_HEIGHT - ball_radius)\
    ]

    # toto je trochu tricky, ale tento vzorec zvoli rychlost lopty na zaklade
    # polomeru lopty. To bolo vyzadovane v zadani.
    # Tento pristup sa niekedy nazyva, pretoze ty sa snazis namapovat jednu hodnotnu z
    # urciteho intervalu na inu hodnotu z ineho intervalu.
    # Jednoduchy priklad moze byt napr. mapovanie percent na desatinne cisla. Povedzme, ze
    # percenta su v intervale od [0, 100] a ty chces vediet, kolko je 50 percent
    # v intervale [0, 1]. Tak aku hodnotu ti vrati funkcia map?*.
    # V tomto pripade my mame interval [MIN_RADIUS, MAX_RADIUS] a hodnotu ball_radius
    # a snazime sa ju namapovat na interval [MIN_SPEED, MAX_SPEED].
    # Pozri toto pre viac info: https://processing.org/reference/map_.html
    # a toto: map https://stackoverflow.com/a/17135426
    speed = ((ball_radius - MIN_RADIUS) / (MAX_RADIUS - MIN_RADIUS))\
            * (MAX_SPEED - MIN_SPEED) + MIN_SPEED

    # chceme, aby rychlost lopty bola v kazdej dimenzii rovnaka
    ball_speed = [speed, speed]

    # bachni loptu do zoznamu
    balls.append(
        Ball(
            ball_radius,
            ball_position,
            ball_speed
        )
    )

# Podarilo sa mi prerobit program tak, aby necrashoval, ked ho vypnes (aby nevypisoval
# chybovu hlasku v konzoli.)
#
# Ide o to, ze ty vytvoris funkciu, ktora vola samu seba az dovtedy, kym Tk interpret
# nezisti, ze ho nieco zabilo (on_escape)
def loop():

    # v kazdom kroku programu uprav poziciu lopt.
    for b in balls:
        b.tick()

    # tato funkcia pocka 1 ms (prvy argument, skus zvysovat hodnotu po tisickach a pozeraj,
    # co sa deje) a zavola funkciu, ktora je v druhom argumente. Co je samotna funkcia loop
    canvas.after(1, loop)

loop()

#* odpoved: 0.5
#** v skutocnosti to je v argumente self je schovany odkaz
# na objekt lopta
